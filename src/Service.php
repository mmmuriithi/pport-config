<?php namespace pPort\Config;
use pPort\Ioc;

class Service extends Ioc\Service
{

    private static $_configs=[];
    public static $counter=0;
    public static $entities=[];
    public static $default_context='app';


    public static function register_entities($entities)
    {
        return static::$entities=$entities;
    }

    /**
    * Singularize a string.
    * Converts a word to english singular form.
    *
    * Usage example:
    * {singularize "people"} # person
    */
    public static function singularize ($params)
    {
        if (is_string($params))
        {
        $word = $params;
        }
        else if (!$word = $params['word'])
        {
        return false;
        }
        $singular = array (
        '/(quiz)zes$/i' => '\\1',
        '/(matr)ices$/i' => '\\1ix',
        '/(vert|ind)ices$/i' => '\\1ex',
        '/^(ox)en/i' => '\\1',
        '/(alias|status)es$/i' => '\\1',
        '/([octop|vir])i$/i' => '\\1us',
        '/(cris|ax|test)es$/i' => '\\1is',
        '/(shoe)s$/i' => '\\1',
        '/(o)es$/i' => '\\1',
        '/(bus)es$/i' => '\\1',
        '/([m|l])ice$/i' => '\\1ouse',
        '/(x|ch|ss|sh)es$/i' => '\\1',
        '/(m)ovies$/i' => '\\1ovie',
        '/(s)eries$/i' => '\\1eries',
        '/([^aeiouy]|qu)ies$/i' => '\\1y',
        '/([lr])ves$/i' => '\\1f',
        '/(tive)s$/i' => '\\1',
        '/(hive)s$/i' => '\\1',
        '/([^f])ves$/i' => '\\1fe',
        '/(^analy)ses$/i' => '\\1sis',
        '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\\1\\2sis',
        '/([ti])a$/i' => '\\1um',
        '/(n)ews$/i' => '\\1ews',
        '/s$/i' => ''
        );
        $irregular = array(
        'person' => 'people',
        'man' => 'men',
        'child' => 'children',
        'sex' => 'sexes',
        'move' => 'moves'
        );
        $ignore = array(
        'equipment',
        'information',
        'rice',
        'money',
        'species',
        'series',
        'fish',
        'sheep',
        'press',
        'sms',
        );
        $lower_word = strtolower($word);
        foreach ($ignore as $ignore_word)
        {
        if (substr($lower_word, (-1 * strlen($ignore_word))) == $ignore_word)
        {
        return $word;
        }
        }
        foreach ($irregular as $singular_word => $plural_word)
        {
        if (preg_match('/('.$plural_word.')$/i', $word, $arr))
        {
        return preg_replace('/('.$plural_word.')$/i', substr($arr[0],0,1).substr($singular_word,1), $word);
        }
        }
        foreach ($singular as $rule => $replacement)
        {
        if (preg_match($rule, $word))
        {
        return preg_replace($rule, $replacement, $word);
        }
        }
        return $word;
    }

    
    public static function pluralize($string)
    {
        $plural = array(
                array( '/(quiz)$/i',               "$1zes"   ),
                    array( '/^(ox)$/i',                "$1en"    ),
                    array( '/([m|l])ouse$/i',          "$1ice"   ),
                    array( '/(matr|vert|ind)ix|ex$/i', "$1ices"  ),
                    array( '/(x|ch|ss|sh)$/i',         "$1es"    ),
                    array( '/([^aeiouy]|qu)y$/i',      "$1ies"   ),
                    array( '/([^aeiouy]|qu)ies$/i',    "$1y"     ),
                    array( '/(hive)$/i',               "$1s"     ),
                    array( '/(?:([^f])fe|([lr])f)$/i', "$1$2ves" ),
                    array( '/sis$/i',                  "ses"     ),
                    array( '/([ti])um$/i',             "$1a"     ),
                    array( '/(buffal|tomat)o$/i',      "$1oes"   ),
                    array( '/(bu)s$/i',                "$1ses"   ),
                    array( '/(alias|status)$/i',       "$1es"    ),
                    array( '/(octop|vir)us$/i',        "$1i"     ),
                    array( '/(ax|test)is$/i',          "$1es"    ),
                    array( '/s$/i',                    "s"       ),
                    array( '/$/',                      "s"       )
            );

            $irregular = array(
                    array( 'move',   'moves'    ),
                    array( 'sex',    'sexes'    ),
                    array( 'child',  'children' ),
                    array( 'man',    'men'      ),
                    array( 'person', 'people'   )
            );

            $uncountable = array(
                    'sheep',
                    'fish',
                    'series',
                    'species',
                    'money',
                    'rice',
                    'information',
                    'equipment'
            );

            // save some time in the case that singular and plural are the same
            if (in_array(strtolower($string), $uncountable))
                return $string;

            // check for irregular singular forms
            foreach ($irregular as $noun) {
                if (strtolower($string) == $noun[0])
                    return $noun[1];
            }

            // check for matches using regular expressions
            foreach ($plural as $pattern) {
                if (preg_match($pattern[0], $string))
                    return preg_replace($pattern[0], $pattern[1], $string);
            }

            return $string;
        }
    


    public function __call($configuration,$args=FALSE)
    {
        if(static::is_entity($configuration))
        {

            $context=static::pluralize($configuration);
            $entity=isset($args[0])?$args[0]:FALSE;
            $configuration=isset($args[1])?$args[1]:FALSE;
            $option=isset($args[2])?$args[2]:FALSE;
            $value=isset($args[3])?$args[3]:FALSE;      
        }
        else
        {
            $context="app";
            $entity="app";          
            $option=isset($args[0])?$args[0]:FALSE;
            $value=isset($args[1])?$args[1]:FALSE;
        }
        return self::configuration($context,$entity,$configuration,$option,$value);
    }

    public static function is_entity($configuration)
    {

        return array_key_exists(ucfirst($configuration),self::entities());
    }

    public static function entities($context=false,$property=false,$entity=false)
    {
        if($context==false && $property==false)
        {
            return static::$entities;
        }
        else
        {
            return ($property)?static::$entities[$context][$property]:static::$entities[$context];
        }
        
    }

    public static function get_file($context,$entity,$configuration=false)
    {
        $context=ucfirst(static::singularize($context));
        $context_path=static::entities($context,'path',$entity);
        if(is_object($context_path))
        {
            $context_path=$context_path($context,$entity,$configuration);
        }
        else
        {
            $context_path=$context_path;
        }
        if(file_exists($context_entity_config_path=$context_path.'/config/'.$entity.'/'.$configuration.'.php'))
        {
            $config_path=$context_entity_config_path;
        }
        elseif(file_exists($context_config_path=$context_path.'/config/'.$configuration.'.php'))
        {
            $config_path=$context_config_path;
        }
        else
        {
            $config_path=static::context_path(static::$default_context,$entity).'/config/'.$configuration.'.php';
        }
        return $config_path;
    }




    public static function configuration($context=FALSE,$entity=FALSE,$configuration=FALSE,$option=FALSE,$value=FALSE)
    {
        $config_path=static::get_file($context,$entity,$configuration);
        if(file_exists($config_path))
        {
            if(!isset(self::$_configs[$context][$entity][$configuration]))
            {
                self::$_configs[$context][$entity][$configuration]=include($config_path);               
            }   
            $config=self::$_configs[$context][$entity];

            if($value!==FALSE)
            {               
                return self::$_configs[$context][$entity][$configuration][$option]=$value;
            }   
            else
            {
                //Check if configuration entity is set
                if(isset($config[$configuration]))
                {
                    if($option!=FALSE)
                    {
                        //Check if configuration entity parameter exists
                        if(isset($config[$configuration][$option]))
                        {
                            //Return configuration entity parameter
                            return $config[$configuration][$option];
                        }
                        else
                        {
                            return FALSE;
                        }
                    }
                    else
                    {
                        //If no config parameter return full config array
                        return $config[$configuration];
                    }
                }
                else
                {
                    return FALSE;
                }
            }                   
        }
        else
        {
            if($context==="app")
            {
                throw new \Exception('Could not find the '.$config_path.' configuration file');
            }
            else
            {
                return FALSE;
            }           
        }
    }
    

}
