<?php namespace pPort\Config;
use pPort\Ioc;
class Accessor extends Ioc\Accessor {

	public $alias='config';
	
    public function register()
    {
    	 return "\\pPort\\Config\\Service";
    }
}