<?php namespace pPort\Config;
use pPort\Ioc;
class Provider extends Ioc\Provider {

	/**
	 * Services provided
	 */
	public function provide($dic)
	{
		$dic->register('config','pPort\Config\Accessor','Config');
	}
}